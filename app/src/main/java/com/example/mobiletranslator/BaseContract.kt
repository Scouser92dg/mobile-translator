package com.example.mobiletranslator


class BaseContract {
    interface Presenter<in T> {
        fun attachView(view: T)
        fun detachView(view: T)
    }

    interface View {
        fun showErrorMessage(message: String)
    }
}