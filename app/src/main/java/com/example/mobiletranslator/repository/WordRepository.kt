package com.example.mobiletranslator.repository

import android.util.Log
import com.example.mobiletranslator.model.DataSource
import com.example.mobiletranslator.model.Word
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class WordRepository @Inject constructor(private val dataSource: DataSource) {

    fun getWords(): Flowable<List<Word>> {
        return Flowable.concat(
            getAllWordsFromDB(),
            getWordsFromApi()
        )
    }

    private fun getAllWordsFromDB(): Flowable<List<Word>> {
        return dataSource.database.wordDao().getAllWords()
            .observeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toFlowable()
            .doOnNext {
                Log.d("WordRepository", "get ${it.size} words from db")
            }
    }

    private fun getWordsFromApi(): Flowable<List<Word>> {
        return dataSource.wordService.getWords()
            .observeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toFlowable()
            .doOnNext {
                Log.d("WordRepository", "fetched ${it.size} words from API")
            }
    }

    fun getWordsByFilter(textFilter: String): Flowable<List<Word>> {
        return dataSource.database.wordDao().getWordsByFilter(textFilter)
            .observeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toFlowable()
            .doOnNext {
                Log.d("WordRepository", "get ${it.size} words from DB")
            }
    }

    fun updateWords(words: List<Word>): Completable {
        return dataSource.database.wordDao().deleteAllWords()
            .andThen(Completable.defer { dataSource.database.wordDao().insertWords(words) } )
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
            .doOnComplete {
                Log.d("WordRepository", "${words.size} words updated")
            }
    }

    fun getWord(polishTranslate: String): Observable<Word> {
        return dataSource.database.wordDao().getWordByPolishTranslate(polishTranslate)
            .observeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .doOnNext {
                Log.d("WordRepository", "get word ${it.polishTranslate}, ${it.englishTranslate} from DB")
            }
    }

}