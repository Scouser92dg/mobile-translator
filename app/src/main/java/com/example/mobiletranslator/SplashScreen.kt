package com.example.mobiletranslator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mobiletranslator.ui.MainActivity
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import java.util.concurrent.TimeUnit

class SplashScreen : AppCompatActivity() {

    lateinit var disposables: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        disposables = CompositeDisposable()

        Observable.just(true).delay (2, TimeUnit.SECONDS).subscribe {
            Intent(this, MainActivity::class.java).run {
                startActivity(this)
            }
        }.addTo(disposables)

    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    override fun onResume() {
        super.onResume()
        supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        supportActionBar?.show()
    }

}
