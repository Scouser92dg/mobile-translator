package com.example.mobiletranslator.ui.details

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.speech.RecognizerIntent
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.mobiletranslator.R
import com.example.mobiletranslator.di.scope.ActivityScope
import com.example.mobiletranslator.ui.model.WordModelWrapper
import com.example.mobiletranslator.ui.words.WordsFragment
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_word_details.*
import java.util.*
import javax.inject.Inject

@ActivityScope
class WordDetailsFragment @Inject constructor() : Fragment(), HasAndroidInjector,
    WordDetailsContract.View, TextToSpeech.OnInitListener {

    @Inject
    lateinit var presenter: WordDetailsContract.Presenter

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    lateinit var textToSpeech: TextToSpeech

    companion object {
        private const val REQ_CODE_SPEECH_INPUT = 1
        private const val MALE_VOICE_NAME = "en-us-x-sfg#male_2-local"
        private const val TTS_ENGINE_PATH = "com.google.android.tts"
        private const val SPEECH_PROMPT_INTENT_LABEL = "speech prompt"
        const val BUNDLE_POLISH_TRANSLATE = "word_polish_translate"
        const val BUNDLE_APP_MODE = "app_mode"
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_word_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        arguments?.getString(BUNDLE_POLISH_TRANSLATE)?.let {
            presenter.fetchWord(it)
        }

        initToolbar()

        val isLearnMode = arguments?.getString(BUNDLE_APP_MODE) == WordsFragment.APP_MODE_LEARN

        if (isLearnMode) {
            wordDetailsVoiceIcon.visibility = View.VISIBLE
        } else {
            wordDetailsEnglishLayout.visibility = View.GONE
            wordDetailsVoiceIcon.visibility = View.GONE
            wordDetailsPolishLabel.setCompoundDrawablesWithIntrinsicBounds(
                null,
                null,
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_record_voice_over_black_24dp
                ),
                null
            )
            wordDetailsPolishLabel.setOnClickListener {
                wordDetailsVoiceIcon.visibility = View.GONE
                readWordUser()
            }
        }
        wordDetailsVoiceIcon.setOnClickListener {
            if (isLearnMode) {
                readWordSpeaker()
            }
        }
    }

    private fun initToolbar() {
        (this.activity as AppCompatActivity).supportActionBar?.let { actionBar ->
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
            }
        }
        return false
    }

    override fun setWord(word: WordModelWrapper) {
        wordDetailsPolishLabel.setText(word.polishTranslate)
        wordDetailsEnglishLabel.setText(word.englishTranslate)

        Glide.with(this@WordDetailsFragment)
            .load(word.imageUrl)
            .centerInside()
            .into(wordDetailsImage)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQ_CODE_SPEECH_INPUT -> {
                if (resultCode == AppCompatActivity.RESULT_OK && null != data) {
                    setVoiceAnswerResult(data)
                }
            }
        }
    }

    private fun setVoiceAnswerResult(data: Intent) {
        val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
        if (result.get(0).toString()
                .toLowerCase(Locale.UK) == wordDetailsEnglishLabel.text.toString()
                .toLowerCase(Locale.UK)
        ) {
            setProperlyAnsweredViews()
        } else {
            setWrongAnsweredViews()
        }
        wordDetailsVoiceIcon.visibility = View.VISIBLE
    }

    private fun setProperlyAnsweredViews() {
        wordDetailsVoiceIcon.setImageDrawable(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.ic_spellcheck_black_24dp
            )
        )
        wordDetailsVoiceIcon.setColorFilter(
            ContextCompat.getColor(
                requireContext(),
                R.color.green
            ), PorterDuff.Mode.SRC_IN
        )
        wordDetailsEnglishLayout.visibility = View.VISIBLE
    }

    private fun setWrongAnsweredViews() {
        wordDetailsEnglishLayout.visibility = View.GONE
        wordDetailsVoiceIcon.setImageDrawable(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.ic_close_black_24dp
            )
        )
        wordDetailsVoiceIcon.setColorFilter(
            ContextCompat.getColor(
                requireContext(),
                R.color.red
            ), PorterDuff.Mode.SRC_IN
        )
    }

    private fun readWordUser() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH)
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, SPEECH_PROMPT_INTENT_LABEL)
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Snackbar.make(
                wordDetailsLayout,
                getString(R.string.word_details_error_troubles_with_speech),
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    private fun readWordSpeaker() {
        textToSpeech = TextToSpeech(requireContext(), this, TTS_ENGINE_PATH)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView(this@WordDetailsFragment)
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            initSpeaker()
        } else {
            showErrorMessage(getString(R.string.word_details_error_failed_init_speaker))
        }
    }

    private fun initSpeaker() {
        val voice = Voice(MALE_VOICE_NAME, Locale("en", "UK"), 400, 200, true, null)
        textToSpeech.setVoice(voice)
        textToSpeech.setSpeechRate(0.8f)

        val result: Int = textToSpeech.setVoice(voice)
        if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            showErrorMessage(getString(R.string.word_details_error_language_not_supported))
        } else {
            speakOut(wordDetailsEnglishLabel.text.toString())
        }
    }

    private fun speakOut(message: String) {
        textToSpeech.speak(message, TextToSpeech.QUEUE_FLUSH, null, null)
    }

    override fun showErrorMessage(message: String) {
        Snackbar.make(wordDetailsLayout, message, Snackbar.LENGTH_SHORT).show()
    }

}