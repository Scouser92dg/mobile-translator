package com.example.mobiletranslator.ui.words

import com.example.mobiletranslator.di.scope.ActivityScope
import com.example.mobiletranslator.di.scope.FragmentScope
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class WordsModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun wordsFragment(): WordsFragment

    @ActivityScope
    @Binds
    abstract fun wordsPresenter(presenter: WordsPresenter): WordsContract.Presenter

}