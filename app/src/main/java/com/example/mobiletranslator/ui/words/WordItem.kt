package com.example.mobiletranslator.ui.words

import android.os.Bundle
import android.view.View
import com.example.mobiletranslator.R
import com.example.mobiletranslator.ui.model.WordModelWrapper
import com.example.mobiletranslator.util.SimpleAbstractItem
import com.mikepenz.fastadapter.diff.DiffCallback
import kotlinx.android.synthetic.main.item_word.view.*

class WordItem(val word: WordModelWrapper, private val selectWord: () -> Unit) : SimpleAbstractItem() {

    override fun bind(view: View) {
        view.wordName.text = word.polishTranslate

        view.setOnClickListener {
            selectWord()
        }
    }

    override val layoutRes: Int = R.layout.item_word

    class WordDiffCallback : DiffCallback<WordItem> {
        override fun areItemsTheSame(oldItem: WordItem, newItem: WordItem): Boolean {
            return true
        }

        override fun areContentsTheSame(oldItem: WordItem, newItem: WordItem): Boolean {
            return oldItem.word.polishTranslate == newItem.word.polishTranslate &&
                    oldItem.word.englishTranslate == newItem.word.englishTranslate
        }

        override fun getChangePayload(
            oldItem: WordItem,
            oldItemPosition: Int,
            newItem: WordItem,
            newItemPosition: Int
        ): Any? {
            val diff = Bundle()
            if (newItem.word.polishTranslate != oldItem.word.polishTranslate) {
                diff.putString("latitude", newItem.word.polishTranslate)
            }
            if (newItem.word.polishTranslate != oldItem.word.polishTranslate) {
                diff.putString("longitude", newItem.word.polishTranslate)
            }
            if (diff.size() == 0) {
                return null
            }
            return diff
        }
    }

}