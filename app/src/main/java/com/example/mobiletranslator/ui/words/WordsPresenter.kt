package com.example.mobiletranslator.ui.words

import com.example.mobiletranslator.di.scope.ActivityScope
import com.example.mobiletranslator.repository.WordRepository
import com.example.mobiletranslator.ui.model.WordModelWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@ActivityScope
class WordsPresenter @Inject constructor(private val wordRepository: WordRepository) :
    WordsContract.Presenter {

    private var view: WordsContract.View? = null
    lateinit var disposables: CompositeDisposable

    override fun fetchWords() {
        wordRepository.getWords()
            .debounce ( 400, TimeUnit.MILLISECONDS )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                view?.setWords(it.map { WordModelWrapper(it) }.sortedBy { it.polishTranslate })
                wordRepository.updateWords(it)
                    .subscribe({}, { error -> error?.message?.let { view?.showErrorMessage(it) }})
            }, { error -> error?.message?.let { view?.showErrorMessage(it) }})
            .addTo(disposables)
    }

    override fun filterWords(textFilter: String) {
        if (textFilter.isNotBlank()) {
            wordRepository.getWordsByFilter(textFilter)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view?.setWords(it.map { WordModelWrapper(it) }.sortedBy { it.polishTranslate })
                }, { error -> error?.message?.let { view?.showErrorMessage(it) }})
                .addTo(disposables)
        } else {
            fetchWords()
        }
    }

    override fun detachView(view: WordsContract.View) {
        this.view = null
        disposables.clear()
    }

    override fun attachView(view: WordsContract.View) {
        this.view = view
        disposables = CompositeDisposable()
    }

}