package com.example.mobiletranslator.ui.details

import com.example.mobiletranslator.BaseContract
import com.example.mobiletranslator.ui.model.WordModelWrapper

class WordDetailsContract {

    interface View : BaseContract.View {
        fun setWord(word: WordModelWrapper)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetchWord(polishTranslate: String)
    }

}