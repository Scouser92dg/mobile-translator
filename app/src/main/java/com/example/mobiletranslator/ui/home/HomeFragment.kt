package com.example.mobiletranslator.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.mobiletranslator.R
import com.example.mobiletranslator.ui.words.WordsFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment  @Inject constructor(): Fragment(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeLearnButton.setOnClickListener {
            goToList(WordsFragment.APP_MODE_LEARN)
        }

        homeTestButton.setOnClickListener {
            goToList(WordsFragment.APP_MODE_TEST)
        }

    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)?.supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)?.supportActionBar?.show()
    }

    private fun goToList(mode: String) {
        activity?.findNavController(R.id.nav_host_fragment)?.let { navController ->
            if (navController.currentDestination?.id == R.id.homeFragment) {
                val bundle = Bundle()
                bundle.putString(WordsFragment.BUNDLE_APP_MODE, mode)
                navController.navigate(R.id.action_homeFragment_to_wordsFragment, bundle)
            }
        }
    }

}