package com.example.mobiletranslator.ui.model

import com.example.mobiletranslator.model.Word

data class WordModelWrapper (
    var englishTranslate: String,
    var polishTranslate: String,
    var imageUrl: String
) {
    constructor(word: Word): this(word.englishTranslate, word.polishTranslate, word.imageUrl)
}