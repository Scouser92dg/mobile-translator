package com.example.mobiletranslator.ui.words

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mobiletranslator.R
import com.example.mobiletranslator.di.scope.ActivityScope
import com.example.mobiletranslator.ui.details.WordDetailsFragment
import com.example.mobiletranslator.ui.model.WordModelWrapper
import com.google.android.material.snackbar.Snackbar
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_words.*
import javax.inject.Inject


@ActivityScope
class WordsFragment @Inject constructor() : Fragment(), HasAndroidInjector, WordsContract.View {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var presenter: WordsContract.Presenter

    private var wordsAdapter = FastItemAdapter<WordItem>()

    companion object {
        const val BUNDLE_APP_MODE = "app_mode"
        const val APP_MODE_LEARN = "mode_learn"
        const val APP_MODE_TEST = "mode_test"
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_words, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()

        wordsRecycler.adapter = wordsAdapter
        wordsRecycler.addItemDecoration(
            DividerItemDecoration(
                wordsRecycler.getContext(),
                LinearLayoutManager.VERTICAL
            )
        )
        activity?.apply{
            presenter.attachView(this@WordsFragment)
            presenter.fetchWords()
        }
    }

    private fun initToolbar() {
        (this.activity as AppCompatActivity).supportActionBar?.let { actionBar ->
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_words, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search -> {
                enableSearch(item)
            }
            android.R.id.home -> {
                activity?.onBackPressed()
            }
        }
        return false
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView(this@WordsFragment)
    }

    override fun setWords(words: List<WordModelWrapper>) {
        FastAdapterDiffUtil.set(
            wordsAdapter.itemAdapter,
            words.map { word ->
                WordItem(word, {
                    arguments?.getString(BUNDLE_APP_MODE)?.let { appMode ->
                        goToDetails(word.polishTranslate, appMode)
                    }
                })
            }, WordItem.WordDiffCallback()
        )
    }

    private fun goToDetails(polishTranslate: String, appMode: String) {
        activity?.findNavController(R.id.nav_host_fragment)?.let { navController ->
            if (navController.currentDestination?.id == R.id.wordsFragment) {
                val bundle = Bundle()
                bundle.putString(WordDetailsFragment.BUNDLE_POLISH_TRANSLATE, polishTranslate)
                bundle.putString(WordDetailsFragment.BUNDLE_APP_MODE, appMode)
                navController.navigate(R.id.action_wordsFragment_to_wordDetailsFragment, bundle)
            }
        }
    }

    private fun enableSearch(item: MenuItem) {
        val searchView: SearchView = item.getActionView() as SearchView
        searchView.setQueryHint(getString(R.string.words_search_view_hint))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { newText ->
                    presenter.filterWords(newText)
                }
                return true
            }
        })
    }

    override fun showErrorMessage(message: String) {
        Snackbar.make(wordsLayout, message, Snackbar.LENGTH_SHORT).show()
    }

}