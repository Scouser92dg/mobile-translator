package com.example.mobiletranslator.ui.details

import com.example.mobiletranslator.di.scope.ActivityScope
import com.example.mobiletranslator.repository.WordRepository
import com.example.mobiletranslator.ui.model.WordModelWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@ActivityScope
class WordDetailsPresenter @Inject constructor(private val repository: WordRepository) :
    WordDetailsContract.Presenter {

    private var view: WordDetailsContract.View? = null
    lateinit var disposables: CompositeDisposable

    override fun fetchWord(polishTranslate: String) {
        repository.getWord(polishTranslate)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                view?.setWord(WordModelWrapper(it))
            }, { error -> error?.message?.let { view?.showErrorMessage(it) }})
            .addTo(disposables)
    }

    override fun detachView(view: WordDetailsContract.View) {
        this.view = null
        disposables.clear()
    }

    override fun attachView(view: WordDetailsContract.View) {
        this.view = view
        disposables = CompositeDisposable()
    }


}