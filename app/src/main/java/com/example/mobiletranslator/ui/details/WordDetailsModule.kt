package com.example.mobiletranslator.ui.details

import com.example.mobiletranslator.di.scope.ActivityScope
import com.example.mobiletranslator.di.scope.FragmentScope
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class WordDetailsModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun wordDetailsFragment(): WordDetailsFragment

    @ActivityScope
    @Binds
    abstract fun wordDetailsPresenter(presenter: WordDetailsPresenter): WordDetailsContract.Presenter

}