package com.example.mobiletranslator.ui.words

import com.example.mobiletranslator.BaseContract
import com.example.mobiletranslator.ui.model.WordModelWrapper

class WordsContract {

    interface View : BaseContract.View {
        fun setWords(events: List<WordModelWrapper>)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetchWords()
        fun filterWords(textFilter: String)
    }

}