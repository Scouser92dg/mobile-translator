package com.example.mobiletranslator.di.module

import com.example.mobiletranslator.di.scope.ActivityScope
import com.example.mobiletranslator.ui.MainActivity
import com.example.mobiletranslator.ui.details.WordDetailsModule
import com.example.mobiletranslator.ui.home.HomeModule
import com.example.mobiletranslator.ui.words.WordsModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = arrayOf(WordsModule::class, WordDetailsModule::class, HomeModule::class))
    abstract fun mainActivity(): MainActivity

}