package com.example.mobiletranslator.di.scope

import javax.inject.Scope


@Scope
@MustBeDocumented
@Retention
annotation class ActivityScope