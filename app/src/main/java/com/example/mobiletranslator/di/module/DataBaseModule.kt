package com.example.mobiletranslator.di.module

import android.app.Application
import androidx.room.Room
import com.example.mobiletranslator.db.AppDatabase
import com.example.mobiletranslator.db.WordDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataBaseModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(application: Application): AppDatabase {
        return Room
            .databaseBuilder(application, AppDatabase::class.java, AppDatabase.DB_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun provideWordDao(appDataBase: AppDatabase): WordDao {
        return appDataBase.wordDao()
    }

}