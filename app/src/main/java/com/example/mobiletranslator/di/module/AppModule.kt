package com.example.mobiletranslator.di.module

import android.content.Context
import com.example.mobiletranslator.BaseApp
import dagger.Binds
import dagger.Module

@Module
abstract class AppModule {

    @Binds
    internal abstract fun bindContext(app: BaseApp): Context

}