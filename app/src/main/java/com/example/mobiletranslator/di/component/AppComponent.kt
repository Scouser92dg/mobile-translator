package com.example.mobiletranslator.di.component

import com.example.mobiletranslator.BaseApp
import com.example.mobiletranslator.di.module.*
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    ActivityBindingModule::class,
    AndroidInjectionModule::class
])
interface AppComponent : AndroidInjector<BaseApp> {

    override fun inject(instance: BaseApp)
    
    @Component.Factory
    interface Factory: AndroidInjector.Factory<BaseApp>
}