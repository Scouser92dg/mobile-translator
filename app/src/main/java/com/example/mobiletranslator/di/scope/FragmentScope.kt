package com.example.mobiletranslator.di.scope

import javax.inject.Scope

@Scope
@Retention
@Target(AnnotationTarget.CLASS,AnnotationTarget.FUNCTION)
annotation class FragmentScope