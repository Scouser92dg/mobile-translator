package com.example.mobiletranslator.api

import com.example.mobiletranslator.api.factory.GsonFactory
import com.example.mobiletranslator.api.factory.OkHttpClientFactory
import com.example.mobiletranslator.api.factory.RetrofitFactory
import com.example.mobiletranslator.model.Word
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.http.GET

interface WordService {

    @GET("getWords")
    fun getWords(): Single<List<Word>>

    companion object {

        @get:Synchronized
        var adapter: Retrofit? = null
            private set

        val wordService: WordService
            @Synchronized get() {
                if (adapter == null) {
                    initRestAdapter()
                }
                return adapter!!.create(WordService::class.java)
            }


        @Synchronized
        private fun initRestAdapter() {
            // use OkHttp client
            val httpClient = OkHttpClientFactory.createOkHttpClient()
            val gson = GsonFactory.createGson()
            adapter = RetrofitFactory.createRetrofit(okHttpClient = httpClient, gson = gson)
        }

        fun getMockedWords(): Single<List<Word>> {
            return Single.just(
                listOf(
                    Word(
                        id = 1,
                        englishTranslate = "word",
                        polishTranslate = "słowo",
                        imageUrl = "image.jpg"
                    ),
                    Word(
                        id = 2,
                        englishTranslate = "word2",
                        polishTranslate = "słowo2",
                        imageUrl = "image2.jpg"
                    ),
                    Word(
                        id = 3,
                        englishTranslate = "word3",
                        polishTranslate = "słowo3",
                        imageUrl = "image3.jpg"
                    ),
                    Word(
                        id = 4,
                        englishTranslate = "word4",
                        polishTranslate = "słowo4",
                        imageUrl = "image4.jpg"
                    )
                )
            )
        }

    }
}