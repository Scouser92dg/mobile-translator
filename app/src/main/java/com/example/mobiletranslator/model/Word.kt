package com.example.mobiletranslator.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity
data class Word (
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id") var id: Int,
    @SerializedName("englishTranslate") var englishTranslate: String,
    @SerializedName("polishTranslate") var polishTranslate: String,
    @SerializedName("imageUrl") var imageUrl: String
)