package com.example.mobiletranslator.model

import android.content.Context
import com.example.mobiletranslator.api.WordService
import com.example.mobiletranslator.db.AppDatabase
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataSource @Inject constructor(context: Context) {
    val database = AppDatabase.getInstance(context)
    val wordService = WordService.wordService
}