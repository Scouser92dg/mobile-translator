package com.example.mobiletranslator.db

import androidx.room.*
import com.example.mobiletranslator.model.Word
import io.reactivex.Completable
import io.reactivex.Single


@Dao
interface WordDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWords(flights: List<Word>): Completable

    @Query("DELETE FROM Word")
    fun deleteAllWords(): Completable

    @Query("SELECT * from Word where polishTranslate = :polishTranslate LIMIT 1")
    fun getWordByPolishTranslate(polishTranslate: String): Single<Word>

    @Query("SELECT * from Word")
    fun getAllWords(): Single<List<Word>>

    @Query("SELECT * from Word where polishTranslate LIKE '%' || :textFilter || '%'")
    fun getWordsByFilter(textFilter: String): Single<List<Word>>

}