package com.example.mobiletranslator

import com.example.mobiletranslator.ui.details.WordDetailsFragmentTest
import com.example.mobiletranslator.ui.home.HomeFragmentTest
import com.example.mobiletranslator.ui.words.WordsFragmentTest
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.Suite.SuiteClasses


@RunWith(Suite::class)
@SuiteClasses(
    HomeFragmentTest::class,
    WordsFragmentTest::class,
    WordDetailsFragmentTest::class
)
class TestsRunner {
}