package com.example.mobiletranslator.ui

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.*
import android.os.Bundle
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.NonNull
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.navigation.NavDeepLinkBuilder
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.*
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.example.mobiletranslator.R
import com.example.mobiletranslator.util.ViewIdlingResource
import com.google.android.material.textfield.TextInputEditText
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.Rule


open class BaseFragmentTest {

    protected val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    protected fun launchFragment(destinationId: Int,
                                 argBundle: Bundle? = null) {
        val launchFragmentIntent = buildLaunchFragmentIntent(destinationId, argBundle)
        activityRule.launchActivity(launchFragmentIntent)
    }

    private fun buildLaunchFragmentIntent(destinationId: Int, argBundle: Bundle?): Intent =
        NavDeepLinkBuilder(InstrumentationRegistry.getInstrumentation().targetContext)
            .setGraph(R.xml.main_navigation)
            .setComponentName(MainActivity::class.java)
            .setDestination(destinationId)
            .setArguments(argBundle)
            .createTaskStackBuilder().intents[0]

    protected fun waitUntilViewIsDisplayed(matcher: Matcher<View?>) {
        val idlingResource: IdlingResource =
            ViewIdlingResource(
                matcher,
                ViewMatchers.isDisplayed()
            )
        try {
            IdlingRegistry.getInstance().register(idlingResource)
            // First call to onView is to trigger the idler.
            Espresso.onView(ViewMatchers.withId(0)).check(ViewAssertions.doesNotExist())
        } finally {
            IdlingRegistry.getInstance().unregister(idlingResource)
        }
    }

    protected fun clickOnViewChild(viewId: Int) = object : ViewAction {
        override fun getConstraints() = null

        override fun getDescription() = "Click on a child view with specified id."

        override fun perform(uiController: UiController, view: View) = ViewActions.click()
            .perform(uiController, view.findViewById<View>(viewId))
    }

    protected fun withMenuIdOrText(@IdRes id: Int, @StringRes menuText: Int): Matcher<View?>? {
        val matcher: Matcher<View?> = ViewMatchers.withId(id)
        return try {
            Espresso.onView(matcher).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            matcher
        } catch (NoMatchingViewException: Exception) {
            Espresso.openActionBarOverflowOrOptionsMenu(
                InstrumentationRegistry.getInstrumentation()
                    .targetContext
            )
            ViewMatchers.withText(menuText)
        }
    }

    protected fun atPosition(
        position: Int,
        @NonNull itemMatcher: Matcher<View?>
    ): Matcher<View?>? {
        checkNotNull(itemMatcher)
        return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder =
                    view.findViewHolderForAdapterPosition(position)
                        ?: // has no item on such position
                        return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }

    private fun sameBitmap(
        actualDrawable: Drawable,
        expectedDrawable: Drawable
    ): Boolean {
        var actualDrawable: Drawable? = actualDrawable
        var expectedDrawable: Drawable? = expectedDrawable
        if (actualDrawable == null || expectedDrawable == null) {
            return false
        }
        if (actualDrawable is StateListDrawable && expectedDrawable is StateListDrawable) {
            actualDrawable = actualDrawable.getCurrent()
            expectedDrawable = expectedDrawable.getCurrent()
        }
        if (actualDrawable is BitmapDrawable) {
            val bitmap = actualDrawable.bitmap
            val otherBitmap = (expectedDrawable as BitmapDrawable).bitmap
            return bitmap.sameAs(otherBitmap)
        }
        if (actualDrawable is VectorDrawable ||
            actualDrawable is VectorDrawableCompat ||
            actualDrawable is GradientDrawable
        ) {
            val drawableRect: Rect = actualDrawable.bounds
            val bitmap = Bitmap.createBitmap(
                drawableRect.width(),
                drawableRect.height(),
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            actualDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
            actualDrawable.draw(canvas)
            val otherBitmap = Bitmap.createBitmap(
                drawableRect.width(),
                drawableRect.height(),
                Bitmap.Config.ARGB_8888
            )
            val otherCanvas = Canvas(otherBitmap)
            expectedDrawable.setBounds(0, 0, otherCanvas.getWidth(), otherCanvas.getHeight())
            expectedDrawable.draw(otherCanvas)
            return bitmap.sameAs(otherBitmap)
        }
        return false
    }

    protected fun withCompoundDrawable(resourceId: Int): Matcher<View?>? {
        return object : BoundedMatcher<View?, TextInputEditText>(TextInputEditText::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has compound drawable resource $resourceId")
            }

            override fun matchesSafely(textInputEditText: TextInputEditText): Boolean {
                for (drawable in textInputEditText.compoundDrawables) {
                    drawable?.let { actualDrawable ->
                        ContextCompat.getDrawable(activityRule.activity, resourceId)?.let { expectedDrawable ->
                            if (sameBitmap(actualDrawable, expectedDrawable)) return true
                        }
                    }
                }
                return false
            }
        }
    }

}