package com.example.mobiletranslator.ui.details

import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withContentDescription
import androidx.test.runner.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.uiautomator.UiSelector
import com.example.mobiletranslator.R
import com.example.mobiletranslator.ui.BaseFragmentTest
import com.example.mobiletranslator.ui.words.WordsFragment
import org.hamcrest.Matchers.not
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class WordDetailsFragmentTest : BaseFragmentTest() {

    @Test
    fun checkIfBackToolbarButtonNavigateToHome() {
        val bundle = Bundle()
        bundle.putString(WordsFragment.BUNDLE_APP_MODE, WordsFragment.APP_MODE_LEARN)
        launchFragment(R.id.wordsFragment, bundle)
        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.wordsLayout))

        Thread.sleep(1000)

        onView(ViewMatchers.withId(R.id.wordsRecycler))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, clickOnViewChild(R.id.wordName)))

        Thread.sleep(1000)

        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.wordDetailsLayout))

        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());

        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.wordsLayout))

        onView(ViewMatchers.withId(R.id.wordsLayout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun checkIfSpeakerSpeechOnVoiceIconClick() {
        val bundle = Bundle()
        bundle.putString(WordDetailsFragment.BUNDLE_APP_MODE, WordsFragment.APP_MODE_LEARN)
        bundle.putString(WordDetailsFragment.BUNDLE_POLISH_TRANSLATE, "widelec")
        launchFragment(R.id.wordDetailsFragment, bundle)
        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.wordDetailsLayout))

        onView(ViewMatchers.withId(R.id.wordDetailsVoiceIcon))
            .perform(click())
    }

    @Test
    fun checkIfViewsFilledOnLearnMode() {
        val bundle = Bundle()
        bundle.putString(WordDetailsFragment.BUNDLE_APP_MODE, WordsFragment.APP_MODE_LEARN)
        bundle.putString(WordDetailsFragment.BUNDLE_POLISH_TRANSLATE, "widelec")
        launchFragment(R.id.wordDetailsFragment, bundle)
        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.wordDetailsLayout))

        onView(ViewMatchers.withId(R.id.wordDetailsPolishLabel))
            .check((ViewAssertions.matches(not(ViewMatchers.withText("")))))

        onView(ViewMatchers.withId(R.id.wordDetailsEnglishLabel))
            .check((ViewAssertions.matches(not(ViewMatchers.withText("")))))

        onView(ViewMatchers.withId(R.id.wordDetailsVoiceIcon))
            .check((ViewAssertions.matches(ViewMatchers.isDisplayed())))
    }

    @Test
    fun checkIfReadWordWorksOnIconClick() {
        val bundle = Bundle()
        bundle.putString(WordDetailsFragment.BUNDLE_APP_MODE, WordsFragment.APP_MODE_TEST)
        bundle.putString(WordDetailsFragment.BUNDLE_POLISH_TRANSLATE, "widelec")
        launchFragment(R.id.wordDetailsFragment, bundle)
        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.wordDetailsLayout))

        onView(ViewMatchers.withId(R.id.wordDetailsPolishLabel))
            .perform(click())

        Thread.sleep(2000)

        val isGoogleDialogDisplayed = device.findObject(UiSelector().descriptionContains("Google")) != null
        assert(isGoogleDialogDisplayed)
    }

    @Test
    fun checkIfViewsFilledOnTestMode() {
        val bundle = Bundle()
        bundle.putString(WordDetailsFragment.BUNDLE_APP_MODE, WordsFragment.APP_MODE_TEST)
        bundle.putString(WordDetailsFragment.BUNDLE_POLISH_TRANSLATE, "widelec")
        launchFragment(R.id.wordDetailsFragment, bundle)
        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.wordDetailsLayout))

        onView(ViewMatchers.withId(R.id.wordDetailsPolishLabel))
            .check((ViewAssertions.matches(not(ViewMatchers.withText("")))))

        onView(ViewMatchers.withId(R.id.wordDetailsEnglishLabel))
            .check((ViewAssertions.matches(not(ViewMatchers.isDisplayed()))))
    }

}