package com.example.mobiletranslator.ui.home

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.runner.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.mobiletranslator.R
import com.example.mobiletranslator.ui.BaseFragmentTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class HomeFragmentTest : BaseFragmentTest() {

    @Before
    fun init() {
        launchFragment(R.id.homeFragment)
        waitUntilViewIsDisplayed(withId(R.id.homeLayout))
    }

    @Test
    fun checkIfLearnButtonNavigateToListOnClick() {
        Espresso.onView(withId(R.id.homeLearnButton))
            .perform(ViewActions.click())

        Thread.sleep(2000)

        Espresso.onView(withId(R.id.wordsRecycler))
            .check(ViewAssertions.matches(isDisplayed()))
    }

    @Test
    fun checkIfTestButtonNavigateToListOnClick() {
        Espresso.onView(withId(R.id.homeTestButton))
            .perform(ViewActions.click())

        Thread.sleep(2000)

        Espresso.onView(withId(R.id.wordsRecycler))
            .check(ViewAssertions.matches(isDisplayed()))
    }

}