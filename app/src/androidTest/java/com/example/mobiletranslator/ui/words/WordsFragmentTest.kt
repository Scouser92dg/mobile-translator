package com.example.mobiletranslator.ui.words

import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.runner.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.mobiletranslator.R
import com.example.mobiletranslator.ui.BaseFragmentTest
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class WordsFragmentTest : BaseFragmentTest() {

    @Test
    fun checkIfBackToolbarButtonNavigateToHome() {
        val bundle = Bundle()
        bundle.putString(WordsFragment.BUNDLE_APP_MODE, WordsFragment.APP_MODE_LEARN)
        launchFragment(R.id.wordsFragment, bundle)
        waitUntilViewIsDisplayed(withId(R.id.wordsLayout))

        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());

        waitUntilViewIsDisplayed(withId(R.id.homeLayout))

        onView(withId(R.id.homeLayout))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkIfSearchWorks() {
        val bundle = Bundle()
        bundle.putString(WordsFragment.BUNDLE_APP_MODE, WordsFragment.APP_MODE_LEARN)
        launchFragment(R.id.wordsFragment, bundle)
        waitUntilViewIsDisplayed(withId(R.id.wordsLayout))

        onView(withMenuIdOrText(R.id.search, R.string.menu_search_label))
            .perform(click())

        Thread.sleep(1000)

        onView(withHint(activityRule.activity.getString(R.string.words_search_view_hint)))
            .perform(ViewActions.typeText("sza"))

        Thread.sleep(2000)

        onView(withId(R.id.wordsRecycler))
            .check(
                matches(
                    atPosition(
                        0,
                        withChild(withText("szafa"))
                    )
                )
            )
    }

    @Test
    fun checkIfNavigateToDetailsLearnModeOnItemClick() {
        val bundle = Bundle()
        bundle.putString(WordsFragment.BUNDLE_APP_MODE, WordsFragment.APP_MODE_LEARN)
        launchFragment(R.id.wordsFragment, bundle)
        waitUntilViewIsDisplayed(withId(R.id.wordsLayout))

        Thread.sleep(1000)

        onView(withId(R.id.wordsRecycler))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, clickOnViewChild(R.id.wordName)))

        Thread.sleep(1000)

        waitUntilViewIsDisplayed(withId(R.id.wordDetailsLayout))

        onView(withId(R.id.wordDetailsEnglishLayout))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkIfNavigateToDetailsTestModeOnItemClick() {
        val bundle = Bundle()
        bundle.putString(WordsFragment.BUNDLE_APP_MODE, WordsFragment.APP_MODE_TEST)
        launchFragment(R.id.wordsFragment, bundle)

        waitUntilViewIsDisplayed(withId(R.id.wordsLayout))

        Thread.sleep(1000)

        onView(withId(R.id.wordsRecycler))
            .perform(
                RecyclerViewActions
                    .actionOnItemAtPosition<RecyclerView.ViewHolder>(0, clickOnViewChild(R.id.wordName)))

        Thread.sleep(1000)

        waitUntilViewIsDisplayed(withId(R.id.wordDetailsLayout))

        onView(withId(R.id.wordDetailsPolishLabel)).check(matches(withCompoundDrawable(R.drawable.ic_record_voice_over_black_24dp)))
    }

}