package com.example.mobiletranslator.repository

import android.accounts.NetworkErrorException
import com.example.mobiletranslator.BaseTest
import com.example.mobiletranslator.api.WordService
import com.example.mobiletranslator.db.AppDatabase
import com.example.mobiletranslator.db.WordDao
import com.example.mobiletranslator.model.DataSource
import com.example.mobiletranslator.model.Word
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.io.IOException


class WordRepositoryTest  : BaseTest() {
    lateinit var repository: WordRepository

    private val dao: WordDao = mock()
    private val service: WordService = mock()
    private val dataSource: DataSource = mock()
    private val appDataBase: AppDatabase = mock()

    private val word = Word(
            id = 1,
            englishTranslate = "word",
            polishTranslate = "słowo",
            imageUrl = "image.jpg"
    )

    private val words = listOf(
        Word(
            id = 1,
            englishTranslate = "word",
            polishTranslate = "słowo",
            imageUrl = "image.jpg"
        ),
        Word(
            id = 2,
            englishTranslate = "word2",
            polishTranslate = "słowo2",
            imageUrl = "image2.jpg"
        ),
        Word(
            id = 3,
            englishTranslate = "word3",
            polishTranslate = "słowo3",
            imageUrl = "image3.jpg"
        ),
        Word(
            id = 4,
            englishTranslate = "word4",
            polishTranslate = "słowo4",
            imageUrl = "image4.jpg"
        )
    )

    @Before
    fun init() {
        repository = WordRepository(dataSource = dataSource)
        whenever(dataSource.database).thenReturn(appDataBase)
        whenever(dataSource.database.wordDao()).thenReturn(dao)
        whenever(dataSource.wordService).thenReturn(service)
    }

    @Test
    fun `should get words when api and dao contains some words`() {
        whenever(service.getWords()).thenReturn(Single.just(words))
        whenever(dao.getAllWords()).thenReturn(Single.just(words))

        repository.getWords()
            .test()
            .assertNoErrors()
            .assertValueAt(0, words)
            .assertValueAt(1, words)

        verify(service).getWords()
        verify(dao).getAllWords()
    }

    @Test
    fun `should get words when api contains some words and dao not`() {
        whenever(service.getWords()).thenReturn(Single.just(words))
        whenever(dao.getAllWords()).thenReturn(Single.just(listOf()))

        repository.getWords()
            .test()
            .assertNoErrors()
            .assertValueAt(0, listOf())
            .assertValueAt(1, words)

        verify(service).getWords()
        verify(dao).getAllWords()
    }

    @Test
    fun `should not get words and return error words when api throw error`() {
        whenever(dao.getAllWords()).thenReturn(Single.just(words))
        whenever(service.getWords()).thenReturn(Single.error(NetworkErrorException("500")))

        repository.getWords()
            .test()
            .assertError(NetworkErrorException::class.java)
    }

    @Test
    fun `should not get words and return error words when dao throw error`() {
        whenever(service.getWords()).thenReturn(Single.just(words))
        whenever(dao.getAllWords()).thenReturn(Single.error(IOException()))

        repository.getWords()
            .test()
            .assertError(IOException::class.java)
    }

    //

    @Test
    fun `should get word when exist in dao`() {
        whenever(dao.getWordByPolishTranslate(any())).thenReturn(Single.just(word))

        repository.getWord("słowo")
            .test()
            .assertNoErrors()
            .assertValue(word)

        verify(dao).getWordByPolishTranslate("słowo")
    }

    @Test
    fun `should not get word and return error when dao throw error`() {
        whenever(dao.getWordByPolishTranslate(any())).thenReturn(Single.error(IOException()))

        repository.getWord("słowo")
            .test()
            .assertError(IOException::class.java)
    }

    @Test
    fun `should get words by filter when exists in dao`() {
        whenever(dao.getWordsByFilter(any())).thenReturn(Single.just(words))

        repository.getWordsByFilter("słow")
            .test()
            .assertNoErrors()
            .assertValue(words)

        verify(dao).getWordsByFilter("słow")
    }

    @Test
    fun `should not get words by filter and return error when dao throw error`() {
        whenever(dao.getWordsByFilter(any())).thenReturn(Single.error(IOException()))

        repository.getWordsByFilter("słow")
            .test()
            .assertError(IOException::class.java)
    }

    @Test
    fun `should update words when dao works fine`() {
        whenever(dao.deleteAllWords()).thenReturn(Completable.complete())
        whenever(dao.insertWords(any())).thenReturn(Completable.complete())

        repository.updateWords(words)
            .test()
            .assertNoErrors()
            .assertComplete()

        verify(dao).deleteAllWords()
        verify(dao).insertWords(words)
    }

}