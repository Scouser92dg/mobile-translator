package com.example.mobiletranslator.ui.details

import com.example.mobiletranslator.BaseTest
import com.example.mobiletranslator.model.Word
import com.example.mobiletranslator.repository.WordRepository
import com.example.mobiletranslator.ui.model.WordModelWrapper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WordDetailsPresenterTest: BaseTest() {

    @Mock
    private lateinit var view: WordDetailsContract.View

    lateinit var presenter: WordDetailsPresenter

    private val wordRepository: WordRepository = mock()
    private val error = Exception("Something wrong")

    private val word = Word(
        id = 1,
        englishTranslate = "word",
        polishTranslate = "słowo",
        imageUrl = "image.jpg"
    )

    @Before
    fun init() {
        whenever(wordRepository.getWord(any())).thenReturn(
            Observable.just(word)
        )

        presenter = WordDetailsPresenter(wordRepository)
        presenter.attachView(view)
    }

    @Test
    fun `should set words when repository works fine`() {
        presenter.fetchWord("słowo")

        Mockito.verify(view).setWord(WordModelWrapper(word))
        Mockito.verify(view, never()).showErrorMessage(error.message!!)
    }

    @Test
    fun `should not set word and show error when repository return error`() {
        whenever(wordRepository.getWord(any())).thenReturn(Observable.error(error))

        presenter.fetchWord("słowo")

        Mockito.verify(view, never()).setWord(WordModelWrapper(word))
        Mockito.verify(view).showErrorMessage(error.message!!)
    }

}
