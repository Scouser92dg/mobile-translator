package com.example.mobiletranslator.ui.words

import com.example.mobiletranslator.BaseTest
import com.example.mobiletranslator.model.Word
import com.example.mobiletranslator.repository.WordRepository
import com.example.mobiletranslator.ui.model.WordModelWrapper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class WordsPresenterTest: BaseTest() {

    @Mock
    private lateinit var view: WordsContract.View

    lateinit var presenter: WordsPresenter

    private val wordRepository: WordRepository = mock()
    private val error = Exception("Something wrong")

    private val words = listOf(
        Word(
            id = 1,
            englishTranslate = "word",
            polishTranslate = "słowo",
            imageUrl = "image.jpg"
        ),
        Word(
            id = 2,
            englishTranslate = "word2",
            polishTranslate = "słowo2",
            imageUrl = "image2.jpg"
        ),
        Word(
            id = 3,
            englishTranslate = "word3",
            polishTranslate = "słowo3",
            imageUrl = "image3.jpg"
        ),
        Word(
            id = 4,
            englishTranslate = "word4",
            polishTranslate = "słowo4",
            imageUrl = "image4.jpg"
        )
    )

    @Before
    fun init() {
        whenever(wordRepository.getWords()).thenReturn(
            Flowable.just(words)
        )
        whenever(wordRepository.getWordsByFilter(any())).thenReturn(Flowable.just(words))
        whenever(wordRepository.updateWords(any())).thenReturn(Completable.complete())

        presenter = WordsPresenter(wordRepository)
        presenter.attachView(view)
    }

    @Test
    fun `should set words when repository works fine`() {
        presenter.fetchWords()

        Mockito.verify(view).setWords(words.map { WordModelWrapper(it) })
        Mockito.verify(view, never()).showErrorMessage(error.message!!)
    }

    @Test
    fun `should not set words and show error when repository return error`() {
        whenever(wordRepository.getWords()).thenReturn(Flowable.error(error))

        presenter.fetchWords()

        Mockito.verify(view, never()).setWords(words.map { WordModelWrapper(it) })
        Mockito.verify(view).showErrorMessage(error.message!!)
    }

    @Test
    fun `should set filtered words when repository works fine`() {
        presenter.filterWords("słow")

        Mockito.verify(view).setWords(words.map { WordModelWrapper(it) })
        Mockito.verify(view, never()).showErrorMessage(error.message!!)
    }

    @Test
    fun `should not set filtered words and show error when repository return error`() {
        whenever(wordRepository.getWordsByFilter(any())).thenReturn(Flowable.error(error))

        presenter.filterWords("słow")

        Mockito.verify(view, never()).setWords(words.map { WordModelWrapper(it) })
        Mockito.verify(view).showErrorMessage(error.message!!)
    }

    @Test
    fun `should show error when update words failed`() {
        whenever(wordRepository.updateWords(any())).thenReturn(Completable.error(error))

        presenter.fetchWords()

        Mockito.verify(view).showErrorMessage(error.message!!)
    }

}